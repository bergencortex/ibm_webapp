from flask import Flask, url_for, render_template, request, flash, redirect, make_response

# Imports for handeling sessions
from flask import session as login_session
import random, string

# Imports for the database models
from model.model_devices import get_dev_id, read_devices
from model.model_timeseries import read_timeseries, insert_measurement, read_last_meas

from time import sleep
import datetime

#import httplib2
import json
from flask import make_response, jsonify
# import requests
# from flask.ext.cors import CORS, cross_origin
app = Flask(__name__)
# cors = CORS(app, resources={r"/foo": {"origins": "*"}})
# app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
def index():
    return render_template('index.html')

sampleJson = """
points = [
    {
        lat: 3,
        lng: 566,
        deviceId: 3,
        value: 25,
    }
]
"""

@app.route('/api/read')
def readData():

    #TODO: send array of dev_id, lat,lng and val
    # First get all the devices
    devices = read_devices()
    # next create empty list and loop over all the devices
    return_values = []
    for dev in devices:
        # get device id TODO: Get the right values
        dev = dev.serialize
        # read last measurement from the device
        dev_list = [dev['lat'],
                    dev['lng'],
                    read_last_meas(
                        dev['id'])]
        # append it to list
        return_values.append(dev_list)
    # finally return it to the requester
    response = make_response('{ "data" : ' + str(return_values) + '}')
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/api/insert', methods=['POST'])
def insertData():
    "Inserts a measurement"
    # First get the request
    meas = request.data # INSERT NAME FROM KOST
    try:
        print(meas)
        # next parse out the params
        jdata = json.loads(meas) #TODO: Parse the data and insert it
        serial = str(jdata.get('deviceuid', "SERIAL"))
        print(str(serial))
        # get dev_id from serial

        dev = get_dev_id(serial)
        if not dev:
            return "no device registered with that serial"
        print("DEVICE ID " + str(dev.id))
        if dev.id % 2 == 0:
            print("got lightmedet data!")
            if insert_measurement(dev.id, float(jdata.get('lightmeter', None))):
                return "Successfully inserted data"
            else:
                return "Failure occured in inserting ts data to db"
        else:
        # and insert into db
            if insert_measurement(dev.id, float(jdata.get('IRtemperature', None)[0])):
                return "Successfully inserted data"
            else:
                return "Failure occured in inserting ts data to db"
    except Exception as e:
        return "An exception occured ----> " + str(e)


return_str = ""
@app.route('/test')
def test():
    global return_str
    # do a loop to insert a few measurements
    return_str = "<h1>Measurements:</h1><br>"
    print(return_str)
    for meas in read_timeseries():
        return_str += str(meas.serialize)
        return_str += '<br>'
    return_str += "<br><h1>Devices</h1><br>"
    print(return_str)
    for dev in read_devices():
        return_str += str(dev.serialize)
        return_str += "<br>"
    return_str += "<br><h1>Select time</h1><br>"
    t1 = datetime.datetime(2017, 11, 5, 13, 45)
    t2 = datetime.datetime(2017, 11, 7, 15, 54)
    # t1 = datetime.time(13, 00)
    # t2 = datetime.time(15, 54)
    # t1 = datetime.date(2017, 11, 5)
    # t2 = datetime.date(2017, 11, 8)
    return_str += "<h3>" + str(t1) + " - " + str(t2) + "</h3><br>"
    for ts in read_timeseries(None, t1, t2):
        # return_str += str(ts.serialize)
        return_str += str(ts.serialize['ts'].date())
        return_str += " ---- " + str(ts.serialize['ts'].time())
        return_str += "<br>"
    return return_str


# @app.route('/api/<int:dev_id>/read')
# def readData(dev_id=None):
#     dev_id = 0 if dev_id == 0 else dev_id
#     # Parse out the time params
#     #get arg = request.args.get('user')
#     print("Got API Request!")
#     get = request.args.get
#     print("GOT API REQUEST ::: ARGS :::")
#     begin_day = get('begin_day') if get('begin_day') is not None else 5
#     begin_hour = get('begin_hour') if get('begin_hour') is not None else 0
#     begin_min = get('begin_min') if get('begin_min') is not None else 0
#     end_day = get('end_day') if get('end_day') is not None else 7
#     end_hour = get('end_hour') if get('end_hour') is not None else 16
#     end_min = get('end_min') if get('end_min') is not None else 50
#     # print req
#     print(begin_day, begin_hour, begin_min, end_day, end_hour, end_min)
#     # Make a datetime object
#     # datetime.datetime(2017, 11, 7, 13, 51, 11, 895203)
#     # (2017, 11, 7, 13, 51, 11, 895203)
#     # year, month, day, hour, minute, second, microsecond,
#     t1 = datetime.datetime(2017, 11, int(begin_day), int(begin_hour), int(begin_min), 0, 0)
#     t2 = datetime.datetime(2017, 11, int(end_day), int(end_hour), int(end_min), 0, 0)
#     # t1 = datetime.date(2017, 11, 5)
#     # t2 = datetime.date(2017, 11, 7)
#     # t1 = datetime.datetime(2017, 11, 5, 13, 45)
#     # t2 = datetime.datetime(2017, 11, 7, 15, 54)
#     # t1 = datetime.time(13, 50)
#     # t2 = datetime.time(15, 54)
#     print("::: TIMES ::::")
#     print(t1)
#     print(str(t2))
#     print("DEVICE ID :: " + str(dev_id))
#     # jsonify
#     for ts in read_timeseries(1, t1, t2):
#         return "t1 is" + str(t1) + str(ts.serialize)
#     return "Nothing between : <br>" + str(t1) + "<br>" + str(t2)



if __name__ == "__main__":
    # seed_db()
    # app.secret_key = "my_secret_key"
    app.debug = True
    app.run(host='0.0.0.0', port = 5000)
