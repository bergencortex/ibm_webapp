import requests

from model.model_timeseries import insert_measurement

def main():
    "Sends a POST req to the insert endpoint"
    data_json = '{"deviceuid":"b0:b4:48:c4:52:84","devicename":"ST-1","lightmeter":130.72}'
    r = requests.post('http://localhost:5000/api/insert', data={'meas': data_json})
    print(dir(r))
    print(r)
    print(r.content)

def test_insert():
    print(insert_measurement(1, 21.124))

if __name__ == '__main__':
    print("posting to server!")
    # test_insert()
    main()
