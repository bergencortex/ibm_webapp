"""Initiates a database session"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def makesession(Base):
    "Initiatees a new session with database, returns the session object "
    # Init the engine obj
    engine = create_engine('postgresql+psycopg2://peakbreaker:culinaryadventure443@139.162.154.82:5432/measurements')
    # Bind engine and the base obj
    Base.metadata.bind = engine
    # Init the session
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    # Return session obj
    return session
