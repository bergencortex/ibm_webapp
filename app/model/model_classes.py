"""Contains the classes for the database models"""

from sqlalchemy import Column, ForeignKey, Integer, String, Float, DateTime
from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

import datetime

Base = declarative_base()


class Devices(Base):
    __tablename__ = 'devices'
    id = Column(Integer, primary_key=True)
    serial = Column(String(30))
    # location data
    lat = Column(Float) #TODO: Float or str
    lng = Column(Float)
    # some info about the device
    name = Column(String(80), nullable=False)
    address = Column(String(250))
    city = Column(String(80))
    state = Column(String(20))
    zipCode = Column(String(10))
    # user_id = Column(Integer, ForeignKey('users.id'))

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'serial':self.serial,
            'lat': self.lat,
            'lng':self.lng,
            'name': self.name,
            'address': self.address,
            'city': self.city,
            'state': self.state,
            'zipCode': self.zipCode
            # 'owner': self.user_id
            }


class Timeseries(Base):
    __tablename__ = 'timeseries_data'
    id = Column(Integer, primary_key=True)
    measurement = Column(Float)
    dev_id = Column(Integer, ForeignKey('devices.id'))
    ts = Column(DateTime, default=datetime.datetime.utcnow)

    @property
    def serialize(self):
        return {
            'id': self.id,
            'measurment': self.measurement,
            'device': self.dev_id,
            'ts': self.ts
            }

# Boiler plate users table
# class Users(Base):
#     __tablename__ = 'users'
#     id = Column(Integer, primary_key=True)
#     name = Column(String(80), nullable=False)
#     email = Column(String(80), nullable=False)
#     picture = Column(String(250))

engine = create_engine('postgresql+psycopg2://peakbreaker:culinaryadventure443@139.162.154.82:5432/measurements')

Base.metadata.create_all(engine)
