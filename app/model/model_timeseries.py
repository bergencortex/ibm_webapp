"""Contains functions for doing CRUD on the Timeseries table"""

from model.model_classes import Base, Timeseries
from model.makesession import makesession

from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound

session = makesession(Base)

def read_last_meas(device_id):
    "gets the last measurement from device"
    try:
        return session.query(Timeseries).order_by(Timeseries.ts.desc()).limit(1).one().measurement #TODO: test this
    except NoResultFound:
        print("found no device with that serial")
        return False

# ----------- BASIC CRUD ---------- BASIC CRUD -----------
def read_timeseries(device_id=None, time_begin=None, time_end=None):
    """
    Lists out timeseries depending on args:
    1. Timeseries on a specific device in device_id is given
    2. Timeseries from time_begin to time_end if both given
    """
    print("----- QUERY FOR TIMESERIES DATA -----")
    # if time isnt specified we just return everything
    if time_begin is None or time_end is None:
        if device_id is None:
            return session.query(Timeseries).all()
        else:
            return session.query(Timeseries).filter_by(dev_id=device_id).all()
    #if time is given, we return based on time
    if device_id is None:
        return session.query(Timeseries).filter((Timeseries.ts).between(time_begin, time_end))
    else: # device id is given
        print("Getting data on devid:: " + str(device_id))
        return session.query(Timeseries).filter_by(dev_id=device_id).\
            filter((Timeseries.ts).between(time_begin, time_end)).all()
        # return session.query(Timeseries).filter_by(dev_id=device_id).\
        #         filter(func.time.as_utc(Timeseries.ts).between(time_begin, time_end))


def insert_measurement(dev_id, measurement):
    """Inserts a new menuitem row in the db"""
    print("Inserting measurement!")
    meas = Timeseries(
                    dev_id=dev_id,
                    measurement=measurement
                    )
    session.add(meas)
    session.commit()
    print("commited data!")
    return True


def delete_measurements(device_id=None, time_begin=None, time_end=None):
    """Deletes the menuitem with the passed id"""
    # meas = None
    if device_id is None:
        meas = session.query(Timeseries).filter(func.time(Timeseries.ts).\
                between(time_begin, time_end))
    else:
        meas = session.query(Timeseries).filter_by(dev_id=device_id).\
                filter(func.time(Timeseries.ts).between(time_begin, time_end))
    for m in meas:
        session.delete(m)
    session.commit()
    # Finally we return the restaurant id
    return True


def delete_all_measurements(dev_id):
        meas = session.query(Timeseries).filter_by(dev_id=dev_id)
        # Then we delete the item from db through the session
        for m in meas:
            session.delete(m)
        session.commit()
        return True
