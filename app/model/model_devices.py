"""Contains models for doing CRUD for devices"""

from model.model_classes import Base, Devices, Timeseries

from model.makesession import makesession
from sqlalchemy.orm.exc import NoResultFound

session = makesession(Base)


def get_dev_id(serial):
    "Takes the serial number of a device and returns device"
    try:
        print("quering for device")
        return session.query(Devices).filter_by(serial=serial).one()
    except NoResultFound:
        print("No results found device with serial" + serial)
        return False


# ----------- BASIC CRUD ---------- BASIC CRUD -----------
def read_devices(deviceid=None):
    """
    Queries the database for devices. Takes deviceid args,
    and returns:
    1. If deviceid == None -> Returns all devices
    """
    if deviceid is None:
        # Getting all devices
        return session.query(Devices).order_by(Devices.name).all()
    else:
        device = session.query(Devices).filter_by(id=deviceid).one()
        # Then we know that the user id is the owner of the device
        return device


def create_device(name, serial, lat, lng, address, city, state, zipCode):
    "Gets parameters for a new devices and adds it into the db"
    device = Devices(
                        name=name,
                        serial=serial,
                        lat=lat,
                        lng=lng,
                        address=address,
                        city=city,
                        state=state,
                        zipCode=zipCode
                      )
    session.add(device)
    session.commit()
    print(":: added device with id ::")
    print(device.id)
    return session.query(Devices).filter_by(serial=serial).one()



def update_device(deviceid, name, address, city, state, zipCode):
    "Updates a device in the db, returns a string if successful"
    device = session.query(Devices).filter_by(id=deviceid).one()
    device.name = name
    device.address = address
    device.city = city
    device.state = state
    device.zipCode = zipCode
    session.add(device)
    session.commit()
    return True


def delete_device(deviceid):
    "Deletes device with belonging menuitems with passed restautant id"
    device = session.query(Devices).filter_by(id=deviceid).one()
    timeseries = session.query(Timeseries).\
        filter_by(device_id=deviceid).all()
    session.delete(timeseries)
    session.delete(device)
    session.commit()
    return True
