# some utils
import random, string

# Imports for the database models
from model.model_devices import read_devices, create_device
from model.model_timeseries import read_timeseries, insert_measurement, \
                                    delete_measurements, delete_all_measurements

from time import sleep
import datetime

#import httplib2
import json
# import requests


def seed_db():
    dev_id = input("seed: dev_id > ")
    if dev_id != "":
        create_device("Tempsens", dev_id,"villaveien7", "bergen", "Hordaland", 7008)
        seed_meas = input("seed meas? (y/n)")
        if seed_meas == 'y':
            insert_measurement(dev_id, 234.23)
        print("seed complete!")
    return


def seed_devices():
    print("Welcome to the wonderful seeding script!")
    while True:
        try:
            # First get user inputs
            ser = input("Serial for device ? > ")
            lat = float(input("lat for device? > "))
            lng = float(input("lng for device? > "))
            # Finally create the device
            print("::Prepared to write this to DB ::\n serial: %s, lat: %s, lng: %s" %
                    (ser, lat, lng))
            if input("Happy with your choices? (y/n) > ") == 'y':
                create_device("Mr.Tempsensor", ser, lat, lng, "NCEPlace", "Bergen", "Hordaland", 7008)
            else:
                print("Try again!")
        except KeyboardInterrupt:
            print("Thanks for the seeds!")
            break
        except Exception as e:
            print("general exception!!! " + str(e))

if __name__ == '__main__':
    seed_devices()
    # clr = input("clear database ? (y/n) > ")
    # if clr == 'y':
    #     for dev in read_devices():
    #         delete_device(dev.id)
    # seed_db()
